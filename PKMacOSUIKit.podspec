
Pod::Spec.new do |spec|

  spec.name         = "PKMacOSUIKit"
  spec.version      = "1.0.2" # Use the CFVersion for version
  spec.summary      = "Framework for rendering UI on the macos os."

  spec.description  = "Framework for rendering UI on the macos os. Currently supports NavigationControlller and TabBarController"

  spec.homepage     = "https://panoskob91@bitbucket.org/panoskob91/pkmacosuikit.git"

  spec.license      = { :type => "MIT" }
  spec.license      = { :type => "MIT", :file => "MIT_LICENCE.txt" }

  spec.author    = "Panagiotis  Kompotis"
  
  spec.platform     = :osx
  spec.platform     = :osx, "10.15"

  #  When using multiple platforms
  # spec.ios.deployment_target = "5.0"
  # spec.osx.deployment_target = "10.7"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"

  spec.source       = { :git => "https://panoskob91@bitbucket.org/panoskob91/pkmacosuikit.git", :tag => "v#{spec.version}" }

  spec.source_files  = "PKMacOSUIKit/**/*.swift"
  spec.exclude_files = "PKMacOSUIKit/Info.plist"
  spec.resources =
    ["PKMacOSUIKit/**/*.xib", "PKMacOSUIKit/*.xcassets"]
  #spec.resource_bundles = {
  #  'PKMacOSUIKit' => ['PKMacOSUIKit/**/*.xib', 'PKMacOSUIKit/*.xcassets']
  #}
  spec.swift_version = '5.0'

  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # spec.dependency "JSONKit", "~> 1.4"

end
