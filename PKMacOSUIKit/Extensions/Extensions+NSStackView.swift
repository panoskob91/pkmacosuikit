//
//  Extensions+NSStackView.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 10/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import AppKit

extension NSStackView {
    func addArrangedSubviews(_ views: [NSView]) {
        for view in views {
            addArrangedSubview(view)
        }
    }
}
