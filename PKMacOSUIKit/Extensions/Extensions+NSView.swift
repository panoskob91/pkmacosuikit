//
//  Extensions+NSView.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 18/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import AppKit

extension NSView {
    func clearAllSubviews() {
        for view in subviews {
            view.removeFromSuperview()
        }
    }
}
