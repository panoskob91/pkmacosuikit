//
//  ViewController.swift
//  PKMacOSUIKit
//
//  Created by Panagiotis  Kompotis  on 21/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import Cocoa

open class PKViewController: NSViewController {
    /// Navigation Controller
    public lazy var navigationViewController: PKNavigationController? = {
        return parent as? PKNavigationController
    }()
     
    /// Navigation bar title
    public override var title: String? {
        didSet {
            navigationViewController?.title = title
        }
    }
    
    open override func viewDidAppear() {
        super.viewDidAppear()
        setupNavigationBar()
    }

    private func setupNavigationBar() {
        guard let navigationController = navigationViewController else {
            return
        }

        //Update navigation bar ui on appear.
        let right = navigationController.rightBarButtons
        navigationController.rightBarButtons = right
        if navigationController.viewControllers.count > 1 {
            navigationController.backButton?.isHidden = false
        } else {
            navigationController.backButton?.isHidden = true
        }
    }
}
