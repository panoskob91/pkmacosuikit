//
//  Constants.swift
//  PKMacOSUIKit
//
//  Created by Panagiotis  Kompotis  on 3/11/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import AppKit

struct Constants {
    //Tab bar
    static let defaultTabBarTintColor = NSColor.systemBlue
    static let defaultTabBarItemTintColor = NSColor.white
    static let defaultTabBarBackgroundColor = NSColor(named: "tab-bar-items-colors")!
    
    //Navigation bar
    static let defaultNavigationBarBackgroundColor = NSColor(named: "nav-bar-colors")!
    static let defaultNavigationBarTextColor = NSColor(named: "nav-bar-text-colors")!
}
