//
//  PKNavigationController.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 26/9/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import AppKit

private enum SlideType {
    case push
    case pop
}

public class PKNavigationController: NSViewController, PKTabbableProtocol {
    
    /// The navigation bar
    public var navigationBar: PKNavigationBar! {
        get {
            if _navBar == nil {
                let subviews = view.subviews
                _navBar = subviews.first as? PKNavigationBar
            }
            return _navBar
        } set {
            navBarView = newValue
            _navBar = newValue
        }
    }
    
    /// The view controllers that are added to the navigationController stack
    public var viewControllers: [NSViewController] = []
    
    /// The controller that is currently displayed
    public var topViewController: NSViewController? {
        viewControllers.last
    }
    
    public var tabBarItem: PKTabBarItem?
    
    /// The title on the navigation bar
    public override var title: String? {
        didSet {
            navigationBar.title = title
        }
    }
    
    /// The back button, on the top left corner
    public var backButton: NSButton? {
        navigationBar.backButton
    }
    
    public var rightBarButtons: [NSButton]? {
        get {
            guard let topVC = topViewController else {
                return nil
            }
            navigationBar.rightBarButtons = _rightButtons[topVC]
            return _rightButtons[topVC]
        } set {
            guard let topVC = topViewController else {
                return
            }
            _rightBarButtons = newValue
            _rightButtons[topVC] = newValue
            navigationBar.rightBarButtons = _rightButtons[topVC]
        }
    }
    
    private var _rightButtons: [NSViewController: [NSButton]] = [:]
    private var _rightBarButtons: [NSButton]?
    private var _navBar: PKNavigationBar!
    @IBOutlet private var navBarView: PKNavigationBar!
    @IBOutlet private var contentView: NSView!
    
    public init(rootViewController rootVC: NSViewController) {
        let bundle = Bundle(for: type(of: self))
        super.init(nibName: "PKNavigationController", bundle: bundle)
        viewControllers.append(rootVC)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        guard let currentViewController = topViewController else { return }
        navigationBar.navigationDelegate = self
        addChild(currentViewController, animated: false)
    }
    
    // MARK: - Private methods

    private func updateContentView(from vc: NSViewController,
                                   animates: Bool,
                                   slideType: SlideType,
                                   completion: ((() -> Void))?) {
        vc.view.autoresizingMask = [NSView.AutoresizingMask.width,
                                                NSView.AutoresizingMask.height]

        //Animate
        if animates {
            animate(view: vc.view, slideType: slideType, duration: 0.6, completion: completion)
        } else {
            vc.view.frame = contentView.bounds
        }
    }
    
    private func animate(view: NSView, slideType: SlideType , duration: TimeInterval, completion: ((() -> Void))?) {
        var startFrame = NSRect.zero
        var endFrame = NSRect.zero
        let width = contentView.frame.width
        let height = contentView.frame.height
        
        switch slideType {
        case .push:
            startFrame = NSRect(x: width,
                                y: 0,
                                width: width,
                                height: height)
            endFrame = contentView.bounds
        case .pop:
            startFrame = contentView.bounds
            endFrame = NSRect(x: width,
                              y: 0,
                              width: width,
                              height: height)
        }
        
        view.frame = startFrame
        NSAnimationContext.runAnimationGroup({ (context) in
            context.duration = duration
            let animator = view.animator()

            animator.frame = endFrame
        }) {
            completion?()
        }
    }
    
    private func index(from vc: NSViewController) -> Int {
        var index: Int = 0
        for i in 0 ..< viewControllers.count {
            if viewControllers[i] == vc {
                index = i
            }
        }

        return index
    }
    
    private func remove(viewController: NSViewController, animates: Bool) -> NSViewController {
        let vcIndex = index(from: viewController)
        //Remove from children array
        removeChild(at: vcIndex)
        //Remove from viewControllers array
        viewControllers.remove(at: index(from: viewController))
        //For appear lifecycle to be triggered, we need first to remove the topVC view from super view, becuase addSubview does not add the same object twice.
        //A solution of triggering appear events after removing subview would be the correct solution, but I could not find a solution like that.
        
        //Animate pop
        updateContentView(from: viewController, animates: true, slideType: .pop) {
            viewController.view.removeFromSuperview()
            self.topViewController?.view.removeFromSuperview()
            self.contentView.addSubview(self.topViewController!.view)
        }
        
        return viewController
    }
    
    private func addChild(_ viewController: NSViewController, animated: Bool) {
        super.addChild(viewController)
        contentView.addSubview(viewController.view)
        updateContentView(from: viewController, animates: animated, slideType: .push, completion: nil)
    }

    // MARK - Public methods
    
    public func push(viewController vc: NSViewController) {
        _rightButtons[vc] = []
        viewControllers.append(vc)
        addChild(vc, animated: true)
    }

    public func pop() -> NSViewController {
        _rightButtons[topViewController!] = nil
        let vc = remove(viewController: topViewController!, animates: true)
        return vc
    }
}

extension PKNavigationController: PKNavigationDelegate {
    public func didPop() {
        let _ = pop()
        title = topViewController?.title
    }
}
