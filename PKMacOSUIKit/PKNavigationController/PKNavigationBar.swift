//
//  PKNavigationBar.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 26/9/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import Cocoa

public protocol PKNavigationDelegate: class {
    func didPop()
}

open class PKNavigationBar: NSView {
    
    @IBOutlet private var backButtonItem: NSButton!
    @IBOutlet private var titleLabel: NSTextField!
    @IBOutlet private var rightButtonsStackView: NSStackView!
    
    public weak var navigationDelegate: PKNavigationDelegate?
    
    /// The title's text color.
    public var textColor: NSColor = Constants.defaultNavigationBarTextColor
    
    /// The back button
    public var backButton: NSButton? {
        backButtonItem
    }
    
    
    /// Title on the navigation bar
    public var title: String? {
        didSet {
            if let vcTitle = title {
                titleLabel.stringValue = vcTitle
            }
        }
    }
    
    var rightBarButtons: [NSButton]? {
        didSet {
            rightButtonsStackView.clearAllSubviews()
            guard let buttons = rightBarButtons else {
                rightButtonsStackView.isHidden = true
                return
            }
            rightButtonsStackView.isHidden = false
            rightButtonsStackView.addArrangedSubviews(buttons)
        }
    }
    
    public var backgroundColor: NSColor = Constants.defaultNavigationBarBackgroundColor {
        didSet {
            layer?.backgroundColor = backgroundColor.cgColor
        }
    }
    
    public override func layout() {
        super.layout()
        layer?.backgroundColor = backgroundColor.cgColor
        titleLabel.textColor = textColor
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        wantsLayer = true
        setupStackView()
    }
    
    override init(frame frameRect: NSRect) {
        title = ""
        super.init(frame: frameRect)
        commonInit()
    }
    
    public required init?(coder: NSCoder) {
        title = ""
        super.init(coder: coder)
        commonInit()
    }
    
    private func setupStackView() {
        rightButtonsStackView.distribution = .fillEqually
        rightButtonsStackView.spacing = 8
        rightButtonsStackView.isHidden = true
    }
    
    private func commonInit() {
        let nibName = NSNib.Name("PKNavigationBar")
        let bundle = Bundle(for: type(of: self))
        var objects: NSArray?
        let loaded = bundle.loadNibNamed(nibName, owner: self, topLevelObjects: &objects)
        if loaded {
            if let view = objects?.first(where: { $0 is NSView }) as? NSView {
                addSubview(view)
                view.frame = bounds
                view.autoresizingMask = [NSView.AutoresizingMask.width,
                                         NSView.AutoresizingMask.height]
            }
        }
    }
    
    @IBAction private func backButtonTapped(_ sender: NSButton) {
        navigationDelegate?.didPop()
    }
}
