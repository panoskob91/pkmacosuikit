//
//  PKMacOSUIKit.h
//  PKMacOSUIKit
//
//  Created by Panagiotis  Kompotis  on 21/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for PKMacOSUIKit.
FOUNDATION_EXPORT double PKMacOSUIKitVersionNumber;

//! Project version string for PKMacOSUIKit.
FOUNDATION_EXPORT const unsigned char PKMacOSUIKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PKMacOSUIKit/PublicHeader.h>


