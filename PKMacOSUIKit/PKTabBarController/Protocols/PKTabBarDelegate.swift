//
//  PKTabBarDelegate.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 17/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

public protocol PKTabBarDelegate: class {
    func tabBar(_ tabBar: PKTabBar, didPressTabBarItem tabBarItem: PKTabBarItem)
}
