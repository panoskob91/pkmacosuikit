//
//  PKTabableProtocol.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 12/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import Foundation

public protocol PKTabbableProtocol {
    
    /// The tab bar item wich will be added to the tab bar
    var tabBarItem: PKTabBarItem? { get set }
}
