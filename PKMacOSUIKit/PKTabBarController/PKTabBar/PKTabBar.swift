//
//  PKTabBar.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 10/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import AppKit

open class PKTabBar: NSView {

    /// Delegate for handling events happening on PKTabBar view
    public weak var tabBarDelegate: PKTabBarDelegate?
    
    /// The tabBarItems objects added to the view.
    private(set) var tabBarItems: [PKTabBarItem] = []
    
    /// The bar backgroundcolor
    public var backgroundColor: NSColor = Constants.defaultTabBarBackgroundColor {
        didSet {
            layer?.backgroundColor = backgroundColor.cgColor
        }
    }

    /// Bar tint color.
    public var tintColor: NSColor = Constants.defaultTabBarTintColor
    @IBOutlet private var tabBarStackView: NSStackView!
    private var tabBarItemViews: [PKTabBarItemView] = []
    private var selectedTabBarItemView: PKTabBarItemView
    //Keep the set tint color, because on select pointer is change to point to a new value. The key is the view controller, and the value is the tint color of the respective controller.
    private var itemViewTintColors: [PKTabBarItemView: NSColor] = [:]
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        wantsLayer = true
        layer?.backgroundColor = backgroundColor.cgColor
    }
    
    public override init(frame frameRect: NSRect) {
        //Default to first item
        selectedTabBarItemView = tabBarItemViews.first ?? PKTabBarItemView()
        super.init(frame: frameRect)
        commonInit()
    }

    public required init?(coder: NSCoder) {
        //Default to first item
        selectedTabBarItemView = tabBarItemViews.first ?? PKTabBarItemView()
        super.init(coder: coder)
        commonInit()
    }

    open override func layout() {
        super.layout()
        layer?.backgroundColor = backgroundColor.cgColor
        //Sets tintColor on non selected tab bar items
        setupTintColorsOnNonSelectedTabs()
    }
    
    func add(_ view: PKTabBarItemView) {
        //Add gesture recogniser to view
        let clickGestureRecogniser = NSClickGestureRecognizer(target: self, action: #selector(PKTabBar.tabPressed(_:)))
        clickGestureRecogniser.numberOfClicksRequired = 1
        view.addGestureRecognizer(clickGestureRecogniser)
        if let tabBarItem = view.tabBarItem() {
            tabBarItems.append(tabBarItem)            
        }
        itemViewTintColors[view] = view.tintColor
        tabBarItemViews.append(view)
        tabBarStackView.addArrangedSubview(view)
        setupDefaultTabBarItemView()
    }
    
    private func setupDefaultTabBarItemView() {
        if let firstTabBarItemView = tabBarItemViews.first {
            setSelectedState(for: firstTabBarItemView)
        }
    }
    
    @objc private func tabPressed(_ gesture: NSGestureRecognizer) {
        guard let view = gesture.view as? PKTabBarItemView else {
            return
        }
        
        setSelectedState(for: view)
        
        //Get the tab bar item pressed
        if let tabBarItem = view.tabBarItem() {
            //Inform delegate for selected item
            tabBarDelegate?.tabBar(self, didPressTabBarItem: tabBarItem)
        }
    }
    
    private func setSelectedState(for view: PKTabBarItemView) {
        //Update selection
        selectedTabBarItemView = view
        
        //Reset colors to the PKTabBar tintColor
        for v in tabBarItemViews {
            v.tintColor = tintColor
        }
        
        //Get tint color on the selected view
        if let selectedColor = itemViewTintColors[selectedTabBarItemView] {
            //Reset to the tint color set from  the PKTabBarItemView object
            selectedTabBarItemView.tintColor = selectedColor
        }
    }

    private func setupTintColorsOnNonSelectedTabs() {
        //Reset colors to the PKTabBar tintColor
        for v in tabBarItemViews {
            if v != selectedTabBarItemView {
                v.tintColor = tintColor
            }
        }
    }
    
    private func commonInit() {
        let nibName = NSNib.Name("PKTabBar")
        let bundle = Bundle(for: type(of: self))
        var objects: NSArray?
        let loaded = bundle.loadNibNamed(nibName, owner: self, topLevelObjects: &objects)
        if loaded {
            if let view = objects?.first(where: { $0 is NSView }) as? NSView {
                addSubview(view)
                view.frame = bounds
                view.autoresizingMask = [NSView.AutoresizingMask.width,
                                         NSView.AutoresizingMask.height]
            }
        }
    }
}
