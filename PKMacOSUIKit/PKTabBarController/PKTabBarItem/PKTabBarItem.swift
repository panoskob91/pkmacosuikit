//
//  PKTabBarItem.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 10/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import AppKit

public class PKTabBarItem: NSObject {
    public var image: NSImage
    public var title: String
    
    public var tintColor: NSColor?

    public init(image: NSImage, title: String) {
        self.image = image
        self.title = title
    }

    
    func tabBarItemView() -> PKTabBarItemView {
        let view = PKTabBarItemView(tabBarItem: self)
        
        //set view properties
        if let color = tintColor {
            view.tintColor = color
        }
        return view
    }

    func isEqual(to tabBarItem: PKTabBarItem) -> Bool {
        let sameImage = image == tabBarItem.image
        let sameTitle = title == tabBarItem.title
        return sameImage && sameTitle
    }
}
