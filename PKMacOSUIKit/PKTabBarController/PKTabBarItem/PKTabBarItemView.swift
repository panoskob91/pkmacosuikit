//
//  PKTabBarItemView.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 10/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import Cocoa

//This should be internal, cause it is framework implementation details
class PKTabBarItemView: NSView {
    
    /// Tint color for the tab bar item
    var tintColor: NSColor {
        get {
            image.contentTintColor = _tintColor
            titleLabel.textColor = _tintColor
            return _tintColor
        } set {
            _tintColor = newValue
            image.contentTintColor = _tintColor
            titleLabel.textColor = _tintColor
        }
    }
    
    private var _tintColor = Constants.defaultTabBarItemTintColor
    @IBOutlet private var image: NSImageView!
    @IBOutlet private var titleLabel: NSTextField!
    
    init(tabBarItem: PKTabBarItem) {
        super.init(frame: NSRect.zero)
        commonInit()
        self.image.image = tabBarItem.image
        self.titleLabel.stringValue = tabBarItem.title
    }
    
    init(frame frameRect: NSRect, tabBarItem: PKTabBarItem) {
        super.init(frame: frameRect)
        self.image.image = tabBarItem.image
        self.titleLabel.stringValue = tabBarItem.title
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wantsLayer = true
        updateTintColor()
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        updateTintColor()
    }
    
    private func updateTintColor() {
        titleLabel.textColor = tintColor
        image.contentTintColor = tintColor
    }
    
    func tabBarItem() -> PKTabBarItem? {
        guard let img = image.image else {
            return nil
        }
        let item = PKTabBarItem(image: img, title: titleLabel.stringValue)
        item.tintColor = tintColor
        return item
    }
    
    private override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        commonInit()
    }
    
    
    private func commonInit() {
        let bundle = Bundle(for: type(of: self))
        let nibName = NSNib.Name("PKTabBarItemView")
        var objects: NSArray?
        let loaded = bundle.loadNibNamed(nibName, owner: self, topLevelObjects: &objects)
        if loaded {
            if let view = objects?.first(where: { $0 is NSView }) as? NSView {
                addSubview(view)
                view.frame = bounds
                view.autoresizingMask = [NSView.AutoresizingMask.width,
                                         NSView.AutoresizingMask.height]
            }
        }
    }
}
