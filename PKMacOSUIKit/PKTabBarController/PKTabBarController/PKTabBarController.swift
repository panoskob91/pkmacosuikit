//
//  PKTabBarController.swift
//  Currency Converter-MacOS
//
//  Created by Panagiotis  Kompotis  on 10/10/20.
//  Copyright © 2020 Panagiotis Kompotis. All rights reserved.
//

import Cocoa

open class PKTabBarController: NSViewController {
    
    // MARK: - Public properties
    
    /// the TabBar attached to the tab bar controller. This is the bottom view. You can access it to style your tab bar view.
    open var tabBar: PKTabBar! {
        get {
            if _tabBar == nil {
                let subviews = view.subviews
                _tabBar = subviews.last as? PKTabBar
            }
            return _tabBar
        } set {
            tabBarView = newValue
            _tabBar = newValue
        }
    }

    private var _tabBar: PKTabBar!
    private var viewControllers: [NSViewController & PKTabbableProtocol]
    private var selectedViewController: NSViewController?
    @IBOutlet private var contentView: NSView!
    @IBOutlet private var tabBarView: PKTabBar!

    // MARK: - Public methods
    
    public init(viewControllers: [NSViewController & PKTabbableProtocol]) {
        self.viewControllers = viewControllers
        let bundle  = Bundle(for: type(of: self))
        super.init(nibName: "PKTabBarController", bundle: bundle)
    }
    
    public required init?(coder: NSCoder) {
        viewControllers = []
        super.init(coder: coder)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tabBarDelegate = self
        
        //Add first vc as default
        guard let firstVC = viewControllers.first else { return }
        selectedViewController = firstVC
        addChild(firstVC)
        updateTabBarItems()
    }
    
    public override func addChild(_ childViewController: NSViewController) {
        super.addChild(childViewController)
        contentView.addSubview(childViewController.view)
        updateContentView(from: childViewController)
    }
    
    // MARK: - Private methods
    
    private func updateContentView(from vc: NSViewController) {
        vc.view.autoresizingMask = [NSView.AutoresizingMask.width, NSView.AutoresizingMask.height]
        vc.view.frame = contentView.bounds
    }

    private func updateTabBarItems() {
        for vc in viewControllers {
            if let tabBarItem = vc.tabBarItem {
                let view = tabBarItem.tabBarItemView()
                tabBar.add(view)
            }
        }
    }

    private func index(at tabBarItem: PKTabBarItem) -> Int {
        let items = tabBar.tabBarItems
        var i = 0
        for index in 0 ..< items.count {
            let itm = items[index]
            if (itm.isEqual(to: tabBarItem)) {
                i = index
                break
            }
        }
        return i
    }
}

// MARK: - PKTabBarDelegate

extension PKTabBarController: PKTabBarDelegate {
    public func tabBar(_ tabBar: PKTabBar, didPressTabBarItem tabBarItem: PKTabBarItem) {
        contentView.clearAllSubviews()
        
        //Get the item index
        let tabIndex = index(at: tabBarItem)
        
        //Get the selected controller
        selectedViewController = viewControllers[tabIndex]
        if let vc = selectedViewController {
            //Add to the children array only if it s not yet added
            if vc.parent != self {
                //Add Child
                addChild(vc)
            } else {
                contentView.addSubview(vc.view)
            }
            
            
            //Load/show the respective controller
            updateContentView(from: vc)
        }
    }
}
